FROM ubuntu:bionic-20200219
LABEL maintainer=root@cri.epita.fr

RUN apt-get update \
    && apt-get install -y aria2 curl \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /app/files
WORKDIR /app
VOLUME /app/files
COPY entrypoint.sh /app

ENV APP_BASE_URL="https://ipxe.pie.cri.epita.fr/cri-pxe-images.s3.cri.epita.fr"
ENV APP_TORRENT_FILES="pie-archlinux.torrent spe-archlinux.torrent sup-archlinux.torrent exec-archlinux.torrent prepare-disk.torrent"
ENV APP_RELOAD_INTERVAL=60

ENTRYPOINT [ "/app/entrypoint.sh" ]
