#!/usr/bin/env python3

# See requirements.txt
import re
import datetime
import matplotlib.pyplot as plt

uploaded_offset = 0.0
uploaded_sum_max = 0.0

axis_time = []
axis_megabytes_uploaded = []

torrent_upload_size = {}

current_date = None

# docker logs --tail="all" seedbox-cri > seedbox-logs.txt
with open('seedbox-logs.txt', 'r') as file:
    for line in file:
        line = line.strip()
        # Download progress torrent information
        if line.startswith('[#'):
            # Grabbing seed ratio from logs
            matches_upload = re.findall(
                '#([a-f0-9]+?) .+ UL:[0-9.]+([KGM]i)?B\(([0-9.]+?)([KGM]?)iB\)',
                line)
            if matches_upload:
                for torrent_hash, _, size, unit in matches_upload:
                    torrent_upload_size[torrent_hash] = float(size)
                    if unit == 'G':  # If we are using GiB instead of MiB
                        torrent_upload_size[torrent_hash] *= 1024
                    if unit == 'K':  # If we are using KiB instead of MiB
                        torrent_upload_size[torrent_hash] /= 1024
        # Status message containing the current date
        elif line.startswith('*** '):
            # Flushing previous data
            if current_date is not None:
                # Offset is added because of hourly docker reboot
                uploaded_sum = uploaded_offset
                for key in torrent_upload_size:
                    uploaded_sum += float(torrent_upload_size[key]) / 1024.0

                if uploaded_sum >= uploaded_sum_max:
                    uploaded_sum_max = uploaded_sum
                    axis_time.append(current_date)
                    axis_megabytes_uploaded.append(uploaded_sum)
            # Parsing the date
            datepart = line[36:len(line) - 4]
            current_date = datetime.datetime.strptime(
                datepart, '%a %b %d %H:%M:%S %Y')
            # Reset torrent downloaded size
            torrent_upload_size = {}
        # When the docker is restarted
        elif 'Reloading torrents' in line:
            uploaded_offset = uploaded_sum_max
            current_date = None

t = plt.plot(axis_time, axis_megabytes_uploaded)

# Autoformat the date
plt.gcf().autofmt_xdate()

# Labels
plt.ylabel('GiB')
plt.xlabel('Time')
plt.title('Total uploaded GiB over time')

plt.show()
