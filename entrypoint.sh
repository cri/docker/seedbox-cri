#! /bin/sh

APP_FILES_LIST=""

for torrent_file in ${APP_TORRENT_FILES}; do
  APP_FILES_LIST="${APP_FILES_LIST} ${APP_BASE_URL}/${torrent_file}"
done

while aria2c -Z --dir=/app/files --seed-time="${APP_RELOAD_INTERVAL}" --seed-ratio=0.0 --allow-overwrite=true --save-session=/app/aria2c.session --save-session-interval=600 --force-save=true ${APP_FILES_LIST}; do
  echo "[$(date --iso=seconds)] Reloading torrents..."
  rm /app/files/*.torrent
done
