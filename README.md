# seedbox-cri

This docker container based on a ubuntu system allows you to seed the most used
images of the PIE.

## Quickstart

First, install [docker](https://www.docker.com) on your machine. To do this, you
can follow the instructions provided by your distribution's documentation or by
the docker documentation.

You can then start the container with the following command:

```
docker run -d --net=host --name seedbox-cri registry.cri.epita.fr/cri/docker/seedbox-cri/master:latest
```

The `--net=host` option will allow the container to open ports on your machine
to seed the torrents.

## Tweaking the open ports

If you don't want to use the `--net-host` option, you can specify a range of
ports to publish and still be able to seed the torrent. To do this, you can
replace the option `--net=host` with `--publish=6881-6999:6881-6999`. Also, have
a look at [aria2's
documentation](https://aria2.github.io/manual/en/html/aria2c.html). **This
option has not been tested. Use at your own risks.**

## Generating a chart of your contribution

If you want to generate a chart showing your contribution over time, you can
find a python scripts ant its requirements in the `chart` directory

For the script to work you will need to retrieve your logs beforehand,
you can use the following command:

```
docker logs --tail="all" seedbox-cri > seedbox-logs.txt
```

Here is an example chart

![](chart/example-chart.png)

## Related projects

You can find a scoreboard of who has been seeding the most at this address:
https://scoreboard-seedbox-cri.risson.space
